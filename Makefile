
bin/prueba: obj/prueba.o obj/Ejemplo.o 
	gcc obj/prueba.o obj/Ejemplo.o -o bin/prueba		#agregue los archivos .o que necesite

obj/prueba.o: src/prueba.c
	gcc -Wall -c -Iinclude/ src/prueba.c -o obj/prueba.o

obj/Ejemplo.o: src/Ejemplo.c
	gcc -Wall -c -Iinclude/ src/Ejemplo.c -o obj/Ejemplo.o

#agregue las reglas que necesite


.PHONY: clean
clean:
	rm bin/* obj/*.o

